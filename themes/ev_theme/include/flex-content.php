<?php
$layouts = array(
    'free_swiper_section',
    'hero_section',
    'modal_swiper_section',
    'text_section',
    'cta_section',
);

$flexContName = '';
if (!isset($fieldName)) {
    $flexContName = 'flex_content_layouts';
} else {
    $flexContName = $fieldName;
}

if (have_rows($flexContName)) {
    while (have_rows($flexContName)) {
        the_row();

        foreach ($layouts as $layout) {
            if (get_row_layout() == $layout) {
                include __DIR__ . '/flex-content/' . $layout . '.php';
            }
        }
    }
}