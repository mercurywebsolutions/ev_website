<?php
$section_classes = '';
$section_style = '';

if ($image = get_sub_field('hero_image')) {
    $section_style = 'style="background-image: url(' . $image['url'] . ')"';
}
?>
<section class="hero-section <?php echo $section_classes; ?>" <?php echo $section_style; ?>>
    <div class="hero-section-sheen"></div>
    <?php if ($field = get_sub_field('hero_heading')) : ?>
    <p class="hero-section-heading"><?php echo $field; ?></p>
    <?php endif; ?>
</section>