<?php
$section_classes = '';
$size = get_sub_field('text_size');
$section_classes .= $size . ' ';
?>
<section class="text-section container <?php echo $section_classes; ?>">
    <div class="text-section-left">
        <?php if ($field = get_sub_field('section_heading')) : ?>
        <h2 class="text-section-heading"><?php echo $field; ?></h2>
        <?php endif; ?>
    </div>
    <div class="text-section-right">
        <?php if ($field = get_sub_field('section_text')) : ?>
        <div class="text-section-text text">
            <?php echo $field; ?>
        </div>
        <?php endif; ?>
        <?php if ($field = get_sub_field('section_link')) : ?>
        <a class="btn text-section-link" href="<?php echo $field['url']; ?>">
            <?php echo $field['title']; ?>
        </a>
        <?php endif; ?>
    </div>
    <?php if ($field = get_sub_field('vertical_text')) : ?>
    <p class="text-section-vertical"><?php echo $field; ?></p>
    <?php endif; ?>
</section>