<?php
$section_classes = '';
?>
<section class="modal-swiper-section container <?php echo $section_classes; ?>">
    <div class="modal-swiper-section-inner">
        <?php if (have_rows('swiper_slides')) : $i = 0; ?>
        <div class="swiper-container modal-swiper-container">
            <div class="swiper-wrapper modal-swiper-wrapper">
                <?php while(have_rows('swiper_slides')) : the_row(); ?>
                <div id="modal-<?php echo $i; ?>" class="swiper-slide modal-swiper-slide">
                    <div class="modal-swiper-slide-header">
                        <?php if ($field = get_sub_field('slide_heading')) : ?>
                        <p class="modal-swiper-slide-heading"><?php echo $field; ?></p>
                        <?php endif; ?>
                        <?php if ($field = get_sub_field('slide_subheading')) : ?>
                        <p class="modal-swiper-slide-subheading"><?php echo $field; ?></p>
                        <?php endif; ?>
                    </div>
                    <?php if ($field = get_sub_field('slide_text')) : ?>
                    <div class="modal-swiper-slide-text text">
                        <?php echo $field; ?>
                    </div>
                    <a href="#modal-<?php echo $i; ?>" class="btn modal-swiper-slide-link" data-modal-open>Read more</a>
                    <?php endif; ?>
                </div>
                <?php $i++; endwhile; ?>
            </div>
        </div>
        <div class="swiper-button-prev modal-swiper-button-prev">&xlarr;</div>
        <div class="swiper-button-next modal-swiper-button-next">&xrarr;</div>
        <?php endif; ?>
    </div>
</section>