<section class="cta-section">
    <div class="cta-section-inner container">
        <?php if ($field = get_sub_field('section_text')) : ?>
        <div class="cta-section-text text">
            <?php echo $field; ?>
        </div>
        <?php endif; ?>
        <?php if ($field = get_sub_field('section_link')) : ?>
        <a class="btn cta-section-link" href="<?php echo $field['url']; ?>">
            <?php echo $field['title']; ?>
        </a>
        <?php endif; ?>
    </div>
</section>