<?php
$type = get_sub_field('content_type');

$el = '';
$slides = '';
if ($type == 'regular') {
    $el = 'div';
    $slides = get_sub_field('swiper_slides');
} else if ($type == 'project') {
    $el = 'a';
    $slides = get_sub_field('projects');
}

$section_classes = '';
?>
<section class="free-swiper-section container full <?php echo $section_classes; ?>">
    <?php if ($slides) : ?>
    <div class="swiper-container free-swiper-container">
        <div class="swiper-wrapper free-swiper-wrapper">
            <?php
            foreach ($slides as $post) :
                if ($type == 'project') { setup_postdata($post); }
            ?>
            <?php
            // set content based on type
            $attrs = '';
            $image = '';
            $title = '';
            $subtitle = '';
            if ($type == 'regular') {
                $image = $post['slide_image'];
                $title = $post['slide_title'];
            } elseif ($type == 'project') {
                $attrs = 'href="' . get_the_permalink() . '"';
                $image = get_the_post_thumbnail_url();
                $title = get_the_title();
                $subtitle = get_field('project_address');
            }
            ?>
            <<?php echo $el; ?> <?php echo $attrs; ?> class="swiper-slide free-swiper-slide">
                <?php if ($image) : ?>
                <img class="free-swiper-slide-image" src="<?php echo $image; ?>">
                <?php endif; ?>
                <?php if ($title) : ?>
                <p class="free-swiper-slide-title"><?php echo $title; ?></p>
                <?php endif; ?>
                <?php if ($subtitle) : ?>
                <p class="free-swiper-slide-subtitle"><?php echo $subtitle; ?></p>
                <?php endif; ?>
            </<?php echo $el; ?>>
            <?php endforeach;
                wp_reset_postdata(); ?>
        </div>
        <div class="swiper-button-prev free-swiper-button"></div>
        <div class="swiper-button-next free-swiper-button"></div>
    </div>
    <?php endif; ?>
</section>