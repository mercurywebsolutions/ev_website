<?php get_header(); ?>

<section class="hero-section project-swiper-section">
    <?php if (have_rows('hero_slides')) : ?>
    <div class="swiper-container project-swiper-container">
        <?php if ($field = get_field('hero_heading')) : ?>
        <p class="hero-section-heading project-swiper-title"><?php echo $field; ?></p>
        <?php endif; ?>
        <div class="swiper-wrapper project-swiper-wrapper">
            <?php while (have_rows('hero_slides')) : the_row(); ?>
            <div class="swiper-slide project-swiper-slide">
                <div class="project-swiper-slide-sheen"></div>
                <?php if ($image = get_sub_field('slide_image')) : ?>
                <img src="<?php echo $image; ?>" class="project-swiper-slide-image">
                <?php endif; ?>
            </div>
            <?php endwhile; ?>
        </div>
        <div class="swiper-pagination"></div>
    </div>
    <?php endif; ?>
</section>
<section class="text-section container small">
    <div class="text-section-left">
        <?php if ($field = get_the_title()) : ?>
        <h2 class="text-section-heading"><?php echo $field; ?></h2>
        <?php endif; ?>
    </div>
    <div class="text-section-right">
        <?php if ($field = get_field('project_description')) : ?>
        <div class="text-section-text project-description text">
            <?php echo $field; ?>
        </div>
        <?php endif; ?>
        <div class="project-details">
            <?php if ($field = get_field('project_address')) : ?>
            <div class="project-detail">
                <p class="project-detail-label">Address</p>
                <p class="project-detail-value"><?php echo $field; ?></p>
            </div>
            <?php endif; ?>
            <?php if ($field = get_field('project_scope')) : ?>
            <div class="project-detail">
                <p class="project-detail-label">Project Scope</p>
                <p class="project-detail-value"><?php echo $field; ?></p>
            </div>
            <?php endif; ?>
            <?php if ($field = get_field('project_architect')) : ?>
            <div class="project-detail">
                <p class="project-detail-label">Architect</p>
                <p class="project-detail-value"><?php echo $field; ?></p>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <p class="text-section-vertical">Project details</p>
</section>
<?php if ($field = get_field('project_map')) : ?>
<section class="text-section container">
    <div class="project-map">
        <?php echo $field; ?>
    </div>
    <p class="text-section-vertical">Location</p>
</section>
<?php endif; ?>
<?php if ($field = get_field('project_testimonial')) : ?>
<section class="text-section container large">
    <div class="text-section-text text project-testimonial">
        <?php echo $field; ?>
    </div>
    <p class="text-section-vertical">Testimonials</p>
</section>
<?php endif; ?>
<?php if ($projects = get_field('related_projects')) : ?>
<section class="project-related-section text-section container">
    <div class="project-related-list">
        <p class="btn project-related-list-link">
            <a href="<?php echo get_permalink(get_page_by_title('Developments')); ?>">&xlarr; Back to Developments</a>
        </p>
        <?php foreach ($projects as $post) : setup_postdata($post); ?>
        <a href="<?php echo get_the_permalink(); ?>" class="project-related-project">
            <?php if ($image = get_the_post_thumbnail_url()) : ?>
            <img src="<?php echo $image; ?>" class="project-related-project-image">
            <?php endif; ?>
            <?php if ($field = get_the_title()) : ?>
            <p class="project-related-project-title"><?php echo $field; ?></p>
            <?php endif; ?>
            <?php if ($field = get_field('project_address')) : ?>
            <p class="project-related-project-address"><?php echo $field; ?></p>
            <?php endif; ?>
        </a>
        <?php endforeach; ?>
    </div>
    <p class="text-section-vertical">Related projects</p>
</section>
<?php endif; ?>

<?php get_footer(); ?>