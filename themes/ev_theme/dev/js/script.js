'use strict';

var scrollSpeed = 800;
var $navbar = jQuery('#navbar');

jQuery(document).ready(function ($) {
    OnResize();

    // Menu //
    // on btn-menu click
    $('#navbar-menu, #navbar-inner-exit').click(function () {
        $('#navbar-inner').toggleClass('open');
        $('body').toggleClass('nav-open');
    });

    $('#home-cta-arrow a').scrollTo({
        destination: $('#image-1'),
        speed: scrollSpeed,
        navbar: $navbar
    })

    $('body.home .btn-nav-home a').scrollTo({
        destination: $('section:first-of-type'),
        speed: scrollSpeed,
        navbar: $navbar
    }).click(function (e) {
        e.preventDefault();
        $('#navbar-inner').toggleClass('open');
        $('body').toggleClass('nav-open');
    });

    // modal swiper
    var modalSwipers = new Swiper('.modal-swiper-container', {
        spaceBetween: 16,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            568: {
                slidesPerView: 2,
            },
            1024: {
                slidesPerView: 3,
            }
        }
    });

    // free swiper
    var freeSwipers = new Swiper('.free-swiper-container', {
        spaceBetween: 20,
        freeMode: true,
        slidesPerView: 'auto',
        grabCursor: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            768: {
                spaceBetween: 50,
            },
        }
    });

    // project swiper
    var projectSwipers = new Swiper('.project-swiper-container', {
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
        },
    });

    // modal
    var modal = new VanillaModal.default({
        clickOutside: true,
    });

    /// On resize of window ///
    $(window).resize(function () {
        // Call everything that must be done initially and on resize
        OnResize();
    });

    /// On scroll of window ///
    $(window).scroll(function () {
        if ($(window).scrollTop() < $('header').outerHeight() - $navbar.outerHeight()) {
            $navbar.addClass('nav-scroll');
        }
        else {
            $navbar.removeClass('nav-scroll');
        }
    });

    $(window).on("load", function () {
    });

});

// Initially and on resize
function OnResize() {
}