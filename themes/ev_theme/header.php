<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1, user-scalable=0" />
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div class="modal">
        <div class="modal-container container">
            <div class="modal-inner">
                <a class="modal-close btn" data-modal-close>&#10005;</a>
                <div class="modal-content modal-swiper-modal"></div>
            </div>
        </div>
    </div>
    <div id="navbar" class="<?php echo is_front_page() ? 'nav-scroll' : '' ?>">
        <button id="navbar-menu" type="button">
            <?php
            include __DIR__ . "/dist/image/" . "hamburger.svg";
            ?>
        </button>
        <a href="<?php echo get_site_url(); ?>" id="navbar-logo"><img id="navbar-logo"
                src="<?php bloginfo('template_directory'); ?>/dist/image/logo.png "></a>
        <a href="/contact" id="navbar-cta">Enquire</a>
        <div id="navbar-inner">
            <img id="navbar-inner-exit" src="<?php bloginfo('template_directory'); ?>/dist/image/exit.png ">
            <?php wp_nav_menu(array('theme_location' => 'main_menu', 'menu_id' => 'navbar-list', 'menu_class' => 'menu container', 'container' => 'nav', 'container-id' => '', 'container_class' => 'nav')); ?>
        </div>
        <div id="nav-outer"></div>
    </div>