<?php get_header(); ?>
<?php
$hero = '';
if ($hero = get_field('header_image')) {
    $hero = $hero;
}
?>

<header id="home-header" style="background-image: url(' <?php echo $hero ?> ')">
    <?php include __DIR__ . "/dist/image/" . "logo.svg"; ?>
    <p id="home-header-heading">Leading by example</p>
</header>

<?php
$fieldName = '1_flex_content_layouts';
include __DIR__ . '/include/flex-content.php';
?>

<div class='container'>
    <div id="home-cta-arrow"><a href="#">&#xFFEC;</a></div>
</div>

<?php
$image = '';
if ($image = get_field('image_1')) {
    $image = $image;
}
?>

<img id="image-1" src="<?php echo $image; ?>">

<?php
$fieldName = '2_flex_content_layouts';
include __DIR__ . '/include/flex-content.php';

$image = '';
if ($image = get_field('image_2')) {
    $image = $image;
}
?>
<img id="image-2" src="<?php echo $image; ?>">

<?php
$fieldName = '3_flex_content_layouts';
include __DIR__ . '/include/flex-content.php';

$image = '';
if ($image = get_field('image_3')) {
    $image = $image;
}
?>

<div class="two-image-section">
    <img id="image-3" src="<?php echo $image; ?>">
    <?php
    $image = '';
    if ($image = get_field('image_4')) {
        $image = $image;
    }
    ?>
    <img id="image-4" src="<?php echo $image; ?>">
</div>
<?php
$fieldName = '4_flex_content_layouts';
include __DIR__ . '/include/flex-content.php';

$fieldName = 'cta_section';
include __DIR__ . '/include/flex-content.php';
?>

<?php get_footer(); ?>