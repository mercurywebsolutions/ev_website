<?php get_header(); ?>

<?php
if (have_posts()) :
    while (have_posts()) : the_post();
        include __DIR__ . '/include/flex-content.php';
    endwhile;
endif;
?>

<section class="text-section contact-section container">
    <address class="text-section-left contact-section-details">
        <p class="contact-section-details-heading">Contact Us</p>
        <?php if ($field = get_field('business_address', 'options')) : ?>
        <div class="contact-section-details-address">
            <?php echo $field; ?>
        </div>
        <?php endif; ?>
        <?php if ($field = get_field('business_email', 'options')) : ?>
        <a href="mailto:<?php echo $field; ?>" target="_blank" class="contact-section-details-email"><?php echo $field; ?></a>
        <?php endif; ?>
        <?php if ($field = get_field('business_phone', 'options')) : ?>
        <a href="tel:<?php echo $field; ?>" class="contact-section-details-phone"><?php echo $field; ?></a>
        <?php endif; ?>
    </address>
    <div class="text-section-right contact-section-form">
        <?php if ($field = get_field('contact_form_text', 'options')) : ?>
        <div class="contact-section-text text-section-text text">
            <?php echo $field; ?>
        </div>
        <?php endif; ?>
        <?php $cf7 = get_field('contact_form', 'options'); ?>
        <?php echo do_shortcode('[contact-form-7 id="' . $cf7->ID . '" title="' . $cf7->title . '"]'); ?>
    </div>
</section>

<?php if ($map = get_field('business_map', 'options')) : ?>
<section class="text-section contact-section-map container">
    <?php echo $map; ?>
</section>
<?php endif; ?>

<?php get_footer(); ?>