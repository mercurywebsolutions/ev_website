<footer>
    <div id="footer-section">
        <div id="footer-section-left">
            <?php if ($field = get_field('footer_heading', 'options')) { ?>
            <p id="footer-section-left-heading"><?php echo $field; ?></p>
            <?php } ?>
            <div id="footer-section-left-content">
                <?php if ($field = get_field('footer_content', 'options')) {
                    echo $field;
                } ?>
            </div>
        </div>
        <div id="footer-section-right">
            <?php if ($field = get_field('facebook_link', 'options')) { ?>
            <a href="<?php echo $field; ?>"><i id="footer-section-right-facebook"
                    class="fab fa-facebook-f social-icons"></i></a>
            <?php } ?>
            <?php if ($field = get_field('instagram_link', 'options')) { ?>
            <a href="<?php echo $field; ?>"><i id="footer-section-right-instagram"
                    class="fab fa-instagram social-icons"></i></a>
            <?php } ?>
            <?php if ($field = get_field('linkedin_link', 'options')) { ?>
            <a href="<?php echo $field; ?>"><i id="footer-section-right-linkedin"
                    class="fab fa-linkedin-in social-icons"></i></a>
            <?php } ?>
        </div>
    </div>

</footer>

<?php wp_footer(); ?>
</body>

</html>